sh minikube-setup.sh
sh clusterrolebinding.sh
cd cockroachdb-scripts
sh cockroachdb.sh
cd ..
sleep 5s
kubectl apply -f nfs-server.yaml
kubectl -n default get svc nfs-service -o json | jq -r .spec.clusterIP
gsed -r 's/(\b[0-9]{1,3}\.){3}[0-9]{1,3}\b'/"`kubectl -n default get svc nfs-service -o json | jq -r .spec.clusterIP`"/ conversion-deployment.yaml > tmp.yaml
cat tmp.yaml > conversion-deployment.yaml
rm tmp.yaml
gsed -r 's/(\b[0-9]{1,3}\.){3}[0-9]{1,3}\b'/"`kubectl -n default get svc nfs-service -o json | jq -r .spec.clusterIP`"/ files-core-deployment.yaml > tmp.yaml
cat tmp.yaml > files-core-deployment.yaml
rm tmp.yaml
kubectl apply -f files-core-deployment.yaml
kubectl apply -f conversion-deployment.yaml
kubectl apply -f security-center-deployment.yaml
kubectl apply -f swagger-gateway-deployment.yaml
kubectl apply -f security-center-hpa.yaml
kubectl apply -f conversion-hpa.yaml
kubectl apply -f ingress.yaml
cd ../vue-frontend
gsed -r 's/(\b[0-9]{1,3}\.){3}[0-9]{1,3}\b'/"`minikube ip`"/ vue.config.js > tmp.yaml
cat tmp.yaml > vue.config.js
rm tmp.yaml
osascript -e 'tell app "Terminal" to do script "minikube dashboard"'
npm install
npm run serve