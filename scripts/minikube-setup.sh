minikube start --cpus=4 --memory=16000 --disk-size=40000mb
minikube addons enable ingress
minikube addons enable metrics-server