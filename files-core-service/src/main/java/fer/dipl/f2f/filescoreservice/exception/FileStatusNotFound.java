package fer.dipl.f2f.filescoreservice.exception;

public class FileStatusNotFound extends RuntimeException{

    public FileStatusNotFound() {
        super();
    }

    public FileStatusNotFound(String message) {
        super(message);
    }

    public FileStatusNotFound(String message, Exception e) {
        super(message, e);
    }
}
