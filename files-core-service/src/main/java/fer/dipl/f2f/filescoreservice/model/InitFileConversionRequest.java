package fer.dipl.f2f.filescoreservice.model;

import fer.dipl.f2f.filescoreservice.model.conversionservice.OutputFormat;
import lombok.Data;

@Data
public class InitFileConversionRequest {
    private Long id;
    private OutputFormat outputFormat;
}
