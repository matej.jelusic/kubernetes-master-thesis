package fer.dipl.f2f.filescoreservice.resource.test;

import fer.dipl.f2f.filescoreservice.resource.ConversionServiceProxy;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Profile;

@Profile("test")
@FeignClient(name = "conversion-service", url = "http://localhost:8070")//
public interface ConversionServiceProxyTestEnv extends ConversionServiceProxy {
}