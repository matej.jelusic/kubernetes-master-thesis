package fer.dipl.f2f.filescoreservice.model.conversionservice;

import fer.dipl.f2f.filescoreservice.model.ServiceInfo;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ConversionTicket {

  private Long fileStatusId;
  private String contentId;
  private ServiceInfo serviceInfo;


}
