package fer.dipl.f2f.filescoreservice.model.conversionservice;

import com.fasterxml.jackson.annotation.JsonValue;

public enum OutputFormat {
  WEBM("video/webm", ".webm"), MP4("video/mp4",".mp4"), PDF("application/pdf",".pdf"), PNG("image/png",".png"), MP3("audio/mp3",".mp3");

  final String extension;

  final String mimeType;

  OutputFormat(String mimeType, String extension) {
    this.mimeType = mimeType;
    this.extension = extension;
  }

  public String getExtension() {
    return extension;
  }

  @JsonValue
  public String getMimeType() {
    return mimeType;
  }
}
