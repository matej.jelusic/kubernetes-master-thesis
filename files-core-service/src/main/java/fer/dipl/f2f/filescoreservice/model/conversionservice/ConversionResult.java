package fer.dipl.f2f.filescoreservice.model.conversionservice;

import lombok.Data;

import java.util.Date;

@Data
public class ConversionResult {

  private ConversionTicket conversionTicket;

  private OutputFormat outputFormat;

  private boolean success;

  private String errorMessage;

  private OutputFile outputFile;

  private Date dateConverted;
}
