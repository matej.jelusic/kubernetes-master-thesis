package fer.dipl.f2f.filescoreservice.service;

import fer.dipl.f2f.filescoreservice.exception.FileStatusNotFound;
import fer.dipl.f2f.filescoreservice.exception.FileStoreException;
import fer.dipl.f2f.filescoreservice.model.*;
import fer.dipl.f2f.filescoreservice.model.conversionservice.ConversionRequest;
import fer.dipl.f2f.filescoreservice.model.conversionservice.ConversionResult;
import fer.dipl.f2f.filescoreservice.model.conversionservice.ConversionTicket;
import fer.dipl.f2f.filescoreservice.model.conversionservice.OutputFormat;
import fer.dipl.f2f.filescoreservice.repository.FileStatusRepository;
import fer.dipl.f2f.filescoreservice.resource.ConversionServiceProxy;
import lombok.extern.log4j.Log4j2;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


@Service
@Log4j2
public class FilesService {

    @Value("${file.system.base.url}")
    private String BASE_URL;

    private final ServiceInfo serviceInfo;

    private final FileStatusRepository fileStatusRepository;


    @Autowired
    JdbcTemplate jdbcTemplate;

    @Resource
    private final ConversionServiceProxy conversionServiceProxy;

    public FilesService(FileStatusRepository fileStatusRepository, ServiceInfo serviceInfo, ConversionServiceProxy conversionServiceProxy) {
        this.fileStatusRepository = fileStatusRepository;
        this.serviceInfo = serviceInfo;
        this.conversionServiceProxy = conversionServiceProxy;
    }


    public FileStatus storeFile(MultipartFile file, Long userId) {
        final String contentId = new RandomString().nextString();
        String relativeFilePath = contentId + "/" + file.getOriginalFilename();

        try {

            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            Path path = Paths.get(BASE_URL + relativeFilePath);

            if (!Files.exists(path.getParent())) {
                Files.createDirectories(path.getParent());
            }
            Files.write(path, bytes);

            FileStatus fileStatus = new FileStatus();

            fileStatus.setContentId(contentId);
            fileStatus.setDateUploaded(new Date());
            fileStatus.setOriginalFile(relativeFilePath);
            fileStatus.setStoredByService(serviceInfo.toString());
            fileStatus.setUserId(userId);

            return fileStatusRepository.save(fileStatus);

        } catch (IOException e) {
            throw new FileStoreException("Error storing file: " + relativeFilePath, e);
        }
    }

    public FileStatus initFileConversion(Long id, OutputFormat outputFormat) throws FileStatusNotFound {
        FileStatus fileStatus = getFileStatus(id);
        ConversionRequest conversionRequest = new ConversionRequest();

        conversionRequest.setContentId(fileStatus.getContentId());
        conversionRequest.setFileStatusId(id);
        conversionRequest.setContentPath(fileStatus.getOriginalFile());
        conversionRequest.setOutputFormat(outputFormat);
        ConversionTicket conversionTicket = conversionServiceProxy.convertFile(conversionRequest);

        fileStatus.setConvertedByService(conversionTicket.getServiceInfo().toString());
        fileStatus.setOutputFormat(outputFormat.getMimeType());

        return fileStatusRepository.save(fileStatus);
    }

    public FileStatus getFileStatus(Long id) {
        return fileStatusRepository.findById(id).orElseThrow(() -> new FileStatusNotFound("File with id: " + id + " not found"));
    }

    public void fileConverted(ConversionResult conversionResult) throws FileStatusNotFound {
        final Long fileStatusId = conversionResult.getConversionTicket().getFileStatusId();
        FileStatus fileStatus = getFileStatus(fileStatusId);
        fileStatus.setDateConverted(conversionResult.getDateConverted());
        fileStatus.setConvertedFile(conversionResult.getOutputFile().getFilePath());
        fileStatus.setConvertedByService(conversionResult.getConversionTicket().getServiceInfo().toString());
        fileStatus.setOutputFormat(conversionResult.getOutputFormat().getMimeType());
        fileStatusRepository.save(fileStatus);
    }

    public ResponseEntity<org.springframework.core.io.Resource> getResourceResponseEntity(Long id) throws IOException {
        final FileStatus fileStatus = getFileStatus(id);

        final Path filePath = Paths.get(BASE_URL + fileStatus.getConvertedFile());

        Path path = filePath.toAbsolutePath();
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filePath.getFileName());
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");
        header.add("fileName", filePath.getFileName().toString());

        return ResponseEntity.ok()
                .headers(header)
                .contentLength(Files.size(path))
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    public Stats getStats() {
        Stats stats = new Stats();
        List<UploadedByStats> uploadedByStatsList = new LinkedList<>();
        List<Map<String,Object>> results = jdbcTemplate.queryForList("SELECT stored_by_service as uploadedBy, COUNT(stored_By_Service) as count FROM file_status GROUP BY stored_By_Service");
        results.forEach(entry -> {
            UploadedByStats uploadedByStats = new UploadedByStats();
            uploadedByStats.setCount((Long) entry.get("count"));
            uploadedByStats.setService((String) entry.get("uploadedBy"));
            uploadedByStatsList.add(uploadedByStats);
        });
        stats.setUploadedByStats(uploadedByStatsList);

        List<ConvertedByStats> convertedByStatsList = new LinkedList<>();
        results = jdbcTemplate.queryForList("SELECT converted_by_service as convertedBy, COUNT(stored_By_Service) as count FROM file_status GROUP BY converted_By_Service");
        results.forEach(entry -> {
            ConvertedByStats convertedByStats = new ConvertedByStats();
            convertedByStats.setCount((Long) entry.get("count"));
            convertedByStats.setService((String) entry.get("convertedBy"));
            convertedByStatsList.add(convertedByStats);
        });
        stats.setConvertedByStats(convertedByStatsList);
        log.info("Stats fetched: " + stats);
        return stats;
    }

    public List<FileStatus> getFilesForUser(Long userId){
        return fileStatusRepository.getAllByUserId(userId);
    }

    public Map<Long,UsersFilesStats> getUsersFilesStats(){
        Map<Long, UsersFilesStats> usersFilesStatsList = new HashMap<>();
        List<Map<String,Object>>  results = jdbcTemplate.queryForList("select user_id, count(*) as count from file_status group by user_id");
        log.info(results);
        results.forEach(entry -> {
            UsersFilesStats usersFilesStats = new UsersFilesStats();
            usersFilesStats.setUploadedFiles((Long) entry.get("count"));
            usersFilesStats.setUserId((Long) entry.get("user_id"));
            usersFilesStatsList.put(usersFilesStats.getUserId(), usersFilesStats);
        });

        return usersFilesStatsList;
    }

}
