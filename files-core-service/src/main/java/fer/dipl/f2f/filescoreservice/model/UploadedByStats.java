package fer.dipl.f2f.filescoreservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class UploadedByStats {
    String service;
    Long count;
}
