package fer.dipl.f2f.filescoreservice.model.conversionservice;

import lombok.Data;

@Data
public class OutputFile {

  private String fileName;
  private String filePath;
}