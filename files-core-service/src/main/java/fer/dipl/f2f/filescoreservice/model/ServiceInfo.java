package fer.dipl.f2f.filescoreservice.model;

import lombok.Data;

@Data
public class ServiceInfo {
    private String serviceHostName;
    private String serviceHostIp;

    @Override
    public String toString(){
        return serviceHostName +";"+ serviceHostIp;
    }
}

