package fer.dipl.f2f.filescoreservice.resource;

import fer.dipl.f2f.filescoreservice.model.conversionservice.ConversionRequest;
import fer.dipl.f2f.filescoreservice.model.conversionservice.ConversionTicket;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


public interface ConversionServiceProxy {
	//http://localhost:8000/currency-exchange/from/USD/to/INR
	@PostMapping("/convert")
	public ConversionTicket convertFile(@RequestBody ConversionRequest conversionRequest);
}