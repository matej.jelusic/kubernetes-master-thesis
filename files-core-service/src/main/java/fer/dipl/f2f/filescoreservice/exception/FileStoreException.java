package fer.dipl.f2f.filescoreservice.exception;

public class FileStoreException extends RuntimeException {

    public FileStoreException() {
        super();
    }

    public FileStoreException(String message) {
        super(message);
    }

    public FileStoreException(String message, Exception e) {
        super(message, e);
    }
}
