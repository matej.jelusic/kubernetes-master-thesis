package fer.dipl.f2f.filescoreservice.resource.prod;


import fer.dipl.f2f.filescoreservice.resource.ConversionServiceProxy;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Profile("!test")
@FeignClient(name = "currency-exchange-service")//Kubernetes Service Name
public interface ConversionServiceProxyTestEnv extends ConversionServiceProxy {
}