package fer.dipl.f2f.filescoreservice.model;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Data
@ToString
@NoArgsConstructor
public class FileStatus {

    @Id
    @GeneratedValue
    private Long id;
    private String contentId;
    private Date dateUploaded;
    private Date dateConverted;
    private String storedByService;
    private String convertedByService;
    private String originalFile;
    private String convertedFile;
    private String outputFormat;
    private Long userId;
}
