package fer.dipl.f2f.filescoreservice.repository;

import fer.dipl.f2f.filescoreservice.model.ConvertedByStats;
import fer.dipl.f2f.filescoreservice.model.FileStatus;
import fer.dipl.f2f.filescoreservice.model.UploadedByStats;
import fer.dipl.f2f.filescoreservice.model.UsersFilesStats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FileStatusRepository extends JpaRepository<FileStatus, Long> {

    @Query("SELECT e.convertedByService as convertedBy, COUNT(e) as count FROM FileStatus e GROUP BY e.convertedByService")
    List<ConvertedByStats> getConvertedByStatus();

    @Query("SELECT e.storedByService as uploadedBy, COUNT(e) as count FROM FileStatus e GROUP BY e.storedByService")
    List<UploadedByStats> getUploadedByStatus();

    List<FileStatus> getAllByUserId(Long userId);
}
