package fer.dipl.f2f.filescoreservice.config;

import fer.dipl.f2f.filescoreservice.model.ServiceInfo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
public class AppConfiguration {
  @Bean
  public ServiceInfo serviceInfo() throws UnknownHostException {
    ServiceInfo serviceInfo = new ServiceInfo();
    serviceInfo.setServiceHostIp(InetAddress.getLocalHost().getHostAddress());
    serviceInfo.setServiceHostName(InetAddress.getLocalHost()
            .getHostName());
    return serviceInfo;
  }
}