package fer.dipl.f2f.filescoreservice.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class Stats {
    List<UploadedByStats> uploadedByStats;
    List<ConvertedByStats> convertedByStats;
}
