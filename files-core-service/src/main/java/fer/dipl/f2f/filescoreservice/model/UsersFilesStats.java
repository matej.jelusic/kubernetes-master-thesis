package fer.dipl.f2f.filescoreservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class UsersFilesStats {
    Long userId;
    Long uploadedFiles;
}
