package fer.dipl.f2f.filescoreservice.model;

import lombok.Data;

@Data
public class ConvertedByStats {
    String service;
    Long count;
}
