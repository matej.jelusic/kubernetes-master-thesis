package fer.dipl.f2f.filescoreservice.controller;


import fer.dipl.f2f.filescoreservice.exception.FileStatusNotFound;
import fer.dipl.f2f.filescoreservice.model.FileStatus;
import fer.dipl.f2f.filescoreservice.model.InitFileConversionRequest;
import fer.dipl.f2f.filescoreservice.model.Stats;
import fer.dipl.f2f.filescoreservice.model.UsersFilesStats;
import fer.dipl.f2f.filescoreservice.model.conversionservice.ConversionResult;
import fer.dipl.f2f.filescoreservice.repository.FileStatusRepository;
import fer.dipl.f2f.filescoreservice.service.FilesService;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;


@RestController
@Log4j2
public class FilesController {

    final
    FilesService filesService;

    final FileStatusRepository fileStatusRepository;

    public FilesController(FilesService filesService, FileStatusRepository fileStatusRepository) {
        this.filesService = filesService;
        this.fileStatusRepository = fileStatusRepository;
    }

    @GetMapping("/")
    public String imHealthy() {
        return "{healthy:true}";
    }

    @PostMapping("files-core/upload")
    public FileStatus fileUpload(@RequestParam("file") MultipartFile file, @RequestHeader("auth-user") String userId){
        log.info("File saving by user: " + userId);
        return filesService.storeFile(file, Long.parseLong(userId));
    }

    @PostMapping("/init-file-conversion")
    public FileStatus initFileConversion(@RequestBody InitFileConversionRequest initFileConversionRequest) throws FileStatusNotFound {
        return filesService.initFileConversion(initFileConversionRequest.getId(), initFileConversionRequest.getOutputFormat());
    }

    @PostMapping("files-core/file-converted")
    public void fileConverted(@RequestBody ConversionResult conversionResult){
        filesService.fileConverted(conversionResult);
    }

    @GetMapping("/file-status/{id}")
    public FileStatus fileConverted(@PathVariable Long id){
        return filesService.getFileStatus(id);
    }

    @RequestMapping(path = "/files-core/download", method = RequestMethod.GET)
    public ResponseEntity<Resource> download(@RequestParam Long id) throws IOException {
        return filesService.getResourceResponseEntity(id);
    }

    @GetMapping(path = "files-core/stats")
    public Stats getStats() {
        log.info("Getting stats");
        return filesService.getStats();
    }

    @GetMapping(path = "files-core/my-files")
    public List<FileStatus> getMyFiles(@RequestHeader("auth-user") String userId){
        return filesService.getFilesForUser(Long.valueOf(userId));
    }

    @GetMapping(path = "files-core/users-files-stats")
    public Map<Long, UsersFilesStats> getUsersFilesStats(@RequestHeader("auth-user") String userId){
        if(Long.valueOf(userId) == 1){
            return filesService.getUsersFilesStats();
        } else{
            return new HashMap<>();
        }
    }
}
