# Summary

Microservice architecture is the most modern approach in application development.
Applications are structured as collections of loosely connected services of fine granularity,
thus achieving greater modularity and parallelization of development. The application
infrastructure is thus more complex and various concepts and tools are used to facilitate
development and to allow development engineers to focus on business logic. Docker is a
tool used to create images and containers, which enables the isolation of an individual
microservice from the environment in which it is located. Such isolated parts of the
application need to be able to be delivered and dynamically scaled. Kubernetes is a container
orchestration tool that also provides other options that solve the infrastructural problems of
microservice architecture. As part of this thesis, an application based on microservice
architecture using the Spring Boot development framework was also built, and the Docker
and Kubernetes tools for instance management were used. A user graphical interface was
also created in VueJs to test the operation of the delivered microservices on the Minikube
Kubernetes cluster.


**Keywords**: *microservice architecture, Kubernetes, Docker, containers*


# Architecture

![Application architecture](Converter-app.png)