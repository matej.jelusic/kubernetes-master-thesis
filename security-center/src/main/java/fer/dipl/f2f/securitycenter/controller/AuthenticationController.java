package fer.dipl.f2f.securitycenter.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fer.dipl.f2f.securitycenter.model.AuthenticationModel;
import fer.dipl.f2f.securitycenter.service.AuthenticationService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
public class AuthenticationController {

    final
    AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @GetMapping("/")
    public String imHealthy() {
        return "{healthy:true}";
    }

    @GetMapping("/security-center/authenticate")
    public ResponseEntity<String> authenticate(@RequestHeader("Authentication") String token) {
        if(authenticationService.validateToken(token)){
            final Long userIdFromToken = authenticationService.getUserIdFromToken(token);
            log.info("Authenticated token: " + token);
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("auth-user", String.valueOf(userIdFromToken));
            return ResponseEntity.ok().headers(httpHeaders).body("success");

        } else {
            log.info("Token is invalid: " + token);
            return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);
        }
    }

}
