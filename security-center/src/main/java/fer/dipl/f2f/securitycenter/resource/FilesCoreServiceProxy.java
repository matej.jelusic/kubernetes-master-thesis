package fer.dipl.f2f.securitycenter.resource;

import fer.dipl.f2f.securitycenter.domain.UsersFilesStats;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import java.util.Map;

@FeignClient(name = "files-core")//Kubernetes Service Name
public interface FilesCoreServiceProxy {

	@GetMapping("files-core/users-files-stats")
	Map<Long, UsersFilesStats> getUsers(@RequestHeader("auth-user") Long userId);
}