package fer.dipl.f2f.securitycenter.exception;

public class InvalidUsernameAndPasswordException extends RuntimeException{
    public InvalidUsernameAndPasswordException() {
    }

    public InvalidUsernameAndPasswordException(String message) {
        super(message);
    }

    public InvalidUsernameAndPasswordException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidUsernameAndPasswordException(Throwable cause) {
        super(cause);
    }
}
