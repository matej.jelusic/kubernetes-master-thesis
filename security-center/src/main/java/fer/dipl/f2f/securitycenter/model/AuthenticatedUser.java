package fer.dipl.f2f.securitycenter.model;

import fer.dipl.f2f.securitycenter.domain.Users;
import lombok.Data;

@Data
public class AuthenticatedUser {
    private Users user;
    private String token;
}
