package fer.dipl.f2f.securitycenter.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class UsersDto {
    Users users;
    UsersFilesStats usersFilesStats;
}
