package fer.dipl.f2f.securitycenter.service;

import fer.dipl.f2f.securitycenter.domain.Users;
import fer.dipl.f2f.securitycenter.domain.UsersDto;
import fer.dipl.f2f.securitycenter.domain.UsersFilesStats;
import fer.dipl.f2f.securitycenter.repository.UsersRepository;
import fer.dipl.f2f.securitycenter.resource.FilesCoreServiceProxy;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service
@Log4j2
public class UsersManagementService {

    private final UsersRepository usersRepository;

    @Resource
    private FilesCoreServiceProxy filesCoreServiceProxy;

    public UsersManagementService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }


    public List<UsersDto> getUsers(Long userId){
        log.info("Call get users: " + userId);
        List<Users> users = usersRepository.findAll();
        log.info(users.get(0));
        Map<Long, UsersFilesStats> usersFilesStats = filesCoreServiceProxy.getUsers(userId);
        log.info(usersFilesStats);
        List<UsersDto> usersDtos = new LinkedList<>();
        users.forEach(users1 -> {
            UsersDto usersDto = new UsersDto();
            usersDto.setUsers(users1);
            usersDto.setUsersFilesStats(usersFilesStats.get(users1.getId()));
            usersDtos.add(usersDto);
        });

        return usersDtos;
    }
}
