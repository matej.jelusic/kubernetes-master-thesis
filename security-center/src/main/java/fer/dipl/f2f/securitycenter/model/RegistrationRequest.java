package fer.dipl.f2f.securitycenter.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class RegistrationRequest {
    @NotEmpty
    private String username;
    @NotEmpty
    private String password;
    private String email;
    private String firstName;
    private String lastName;
}
