package fer.dipl.f2f.securitycenter.repository;

import fer.dipl.f2f.securitycenter.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users, Long> {

    int countByUsername(String username);
    boolean existsByUsername(String username);
    Users findFirstByUsername(String username);
}
