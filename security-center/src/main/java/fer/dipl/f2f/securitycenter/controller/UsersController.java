package fer.dipl.f2f.securitycenter.controller;


import fer.dipl.f2f.securitycenter.domain.UsersDto;
import fer.dipl.f2f.securitycenter.model.LoginRequest;
import fer.dipl.f2f.securitycenter.model.AuthenticatedUser;
import fer.dipl.f2f.securitycenter.model.RegistrationRequest;
import fer.dipl.f2f.securitycenter.service.AuthenticationService;
import fer.dipl.f2f.securitycenter.service.UsersManagementService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@Log4j2
public class UsersController {

    private final AuthenticationService authenticationService;

    private final UsersManagementService usersManagementService;

    public UsersController(AuthenticationService authenticationService, UsersManagementService usersManagementService) {
        this.authenticationService = authenticationService;
        this.usersManagementService = usersManagementService;
    }

    @PostMapping("/security-center/register")
    public AuthenticatedUser register(@RequestBody @Valid @NotNull RegistrationRequest registrationRequest){
        return authenticationService.register(registrationRequest);
    }

    @PostMapping("/security-center/login")
    public AuthenticatedUser login(@RequestBody @NotEmpty @Valid LoginRequest loginRequest){
        return authenticationService.login(loginRequest);
    }

    @GetMapping("/user-management/users")
    public List<UsersDto> getUsers(@RequestHeader("auth-user") String userId){
        return usersManagementService.getUsers(Long.valueOf(userId));
    }
}
