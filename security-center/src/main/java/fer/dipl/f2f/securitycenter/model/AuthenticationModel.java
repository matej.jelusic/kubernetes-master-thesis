package fer.dipl.f2f.securitycenter.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class AuthenticationModel {
    private Long id;
    private String username;
    private String password;
}
