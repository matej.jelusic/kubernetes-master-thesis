package fer.dipl.f2f.securitycenter.resource.test;

import fer.dipl.f2f.securitycenter.resource.FilesCoreServiceProxy;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Profile;

@Profile("test")
@FeignClient(name = "files-core-service", url = "http://localhost:8050")//
public interface FilesCoreServiceProxyTestEnv extends FilesCoreServiceProxy {
}