package fer.dipl.f2f.securitycenter.service;

import fer.dipl.f2f.securitycenter.domain.Users;
import fer.dipl.f2f.securitycenter.exception.InvalidJwtToken;
import fer.dipl.f2f.securitycenter.exception.InvalidUsernameAndPasswordException;
import fer.dipl.f2f.securitycenter.exception.UsernameAlreadyExistsException;
import fer.dipl.f2f.securitycenter.model.LoginRequest;
import fer.dipl.f2f.securitycenter.model.AuthenticatedUser;
import fer.dipl.f2f.securitycenter.model.RegistrationRequest;
import fer.dipl.f2f.securitycenter.repository.UsersRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AuthenticationService {

    private final JwtTokenUtil jwtTokenUtil;

    private final UsersRepository usersRepository;

    public AuthenticationService(JwtTokenUtil jwtTokenUtil, UsersRepository usersRepository) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.usersRepository = usersRepository;
    }

    public AuthenticatedUser login(LoginRequest loginRequest) {
        log.info("Login attempt: " + loginRequest.getUsername());
        final Users user = usersRepository.findFirstByUsername(loginRequest.getUsername());
        if (isAuthenticated(loginRequest, user)) {
            log.info("User: " + loginRequest.getUsername() + " successfully logged in.");
            return getLoginResponse(user);
        } else {
            throw new InvalidUsernameAndPasswordException("Invalid credentials");
        }
    }

    private AuthenticatedUser getLoginResponse(Users user) {
        final String token = jwtTokenUtil.generateToken(user);
        AuthenticatedUser authenticatedUser = new AuthenticatedUser();
        authenticatedUser.setToken(token);
        authenticatedUser.setUser(user);
        return authenticatedUser;
    }

    private boolean isAuthenticated(LoginRequest loginRequest, Users user) {
        return user != null && user.getPassword() != null && loginRequest.getPassword() != null && loginRequest.getPassword().equals(user.getPassword());
    }


    public AuthenticatedUser register(RegistrationRequest registrationRequest) {
        if(usersRepository.existsByUsername(registrationRequest.getUsername())){
            throw new UsernameAlreadyExistsException("Username: " + registrationRequest.getUsername() + " already exists");
        }

        Users users = new Users();
        users.setUsername(registrationRequest.getUsername());
        users.setPassword(registrationRequest.getPassword());
        users.setEmail(registrationRequest.getEmail());
        users.setFirstName(registrationRequest.getFirstName());
        users.setLastName(registrationRequest.getLastName());
        final Users savedUser = usersRepository.save(users);

        log.info("Saved user with id: " + savedUser.getId() + " and username: " + savedUser.getUsername());

        return getLoginResponse(savedUser);
    }

    public boolean validateToken(String token) {
        try {
            log.info("Parsing token: " + token);
            final String usernameFromToken = jwtTokenUtil.getUsernameFromToken(token);
            log.info("Username from token: " + usernameFromToken);
            return usersRepository.existsByUsername(usernameFromToken);
        } catch (Exception e){
            throw new InvalidJwtToken("Invalid token provided", e);
        }
    }

    public Long getUserIdFromToken(String token) {
        try {
            log.info("Parsing token: " + token);
            final String usernameFromToken = jwtTokenUtil.getUsernameFromToken(token);
            log.info("Username from token: " + usernameFromToken);
            return usersRepository.findFirstByUsername(usernameFromToken).getId();
        } catch (Exception e){
            throw new InvalidJwtToken("Invalid token provided", e);
        }
    }
}
