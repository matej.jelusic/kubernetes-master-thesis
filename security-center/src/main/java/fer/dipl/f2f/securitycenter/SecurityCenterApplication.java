package fer.dipl.f2f.securitycenter;

import fer.dipl.f2f.securitycenter.domain.Users;
import fer.dipl.f2f.securitycenter.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@SpringBootApplication
@EnableFeignClients("fer.dipl.f2f.securitycenter.resource")
@EnableSwagger2
public class SecurityCenterApplication {

    @Autowired
    private UsersRepository usersRepository;


    public static void main(String[] args) {
        SpringApplication.run(SecurityCenterApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        System.out.println("hello world, I have just started up");
        Users users = new Users();
        users.setFirstName("admin");
        users.setLastName("admin");
        users.setEmail("admin@admin");
        users.setPassword("admin");
        users.setUsername("admin");

        if(usersRepository.countByUsername("admin") == 0){
            usersRepository.save(users);
        }
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.ant("/security-center/*"))
                .build().apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Security center microservice",
                "Microservice responsible for authentication and user management",
                "1.0",
                "Terms of service",
                new Contact("Matej Jelušić", "www.fer.hr", "matej.jelusic@fer.hr"),
                "License of API", "API license URL", Collections.emptyList());
    }
}
