package fer.dipl.f2f.securitycenter.resource.prod;


import fer.dipl.f2f.securitycenter.resource.FilesCoreServiceProxy;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Profile;

@Profile("!test")
@FeignClient(name = "files-core")//Kubernetes Service Name
public interface FilesCoreServiceProxyTestEnv extends FilesCoreServiceProxy {
}