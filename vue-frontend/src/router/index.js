import Vue from 'vue'
import VueRouter from 'vue-router'
import FileManager from "../components/FileManager/FileManager";
import Login from "../components/Login/Login";
import Register from "../components/Register/Register";
import FileConvert from "../components/FileConvert/FileConvert";
import Statistics from "../components/Statistics/Statistics";
import {getLoggedUser} from "../util/auth";
import MyFiles from "../components/MyFiles/MyFiles";
import UsersList from "../components/UsersList/UsersList";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'FileConvert',
        component: FileConvert,
        meta: {
            requiresAuth: true,
            admin: false
        }
    },
    {
        path: '/home',
        name: 'FileConvertHome',
        component: FileConvert,
        meta: {
            requiresAuth: true,
            admin: false
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: {
            requiresAuth: false,
            admin: false
        }
    },
    {
        path: '/register',
        name: 'Register',
        component: Register,
        meta: {
            requiresAuth: false,
            admin: false
        }
    },
    {
        path: '/test',
        name: 'Test',
        component: FileManager,
        meta: {
            requiresAuth: false,
            admin: false
        }
    },
    {
        path: '/stats',
        name: 'Statistics',
        component: Statistics,
        meta: {
            requiresAuth: true,
            admin:true
        }
    },
    {
        path: '/my-files',
        name: 'MyFiles',
        component: MyFiles,
        meta: {
            requiresAuth: true,
            admin:false
        }
    },
    {
        path: '/users',
        name: 'UsersList',
        component: UsersList,
        meta: {
            requiresAuth: true,
            admin:true
        }
    }
]

const router = new VueRouter({
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    console.log(to.matched)

    if (to.matched.some(record => record.meta.requiresAuth)) {
        if(to.matched.some(record => record.meta.admin)) {
            if(getLoggedUser().user.user.username === 'admin')
            next()
        } else if (localStorage.getItem('loggedUser') == null)  {
            next({
                path: '/login',
                params: {nextUrl: to.fullPath}
            })
        } else {
            next()
        }
    } else {
        next()
    }
})

export default router