import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)
export const store = new Vuex.Store({
  state: {
    isLoggedIn: !!localStorage.getItem("loggedUser"),
    user: JSON.parse(localStorage.getItem("loggedUser"))
  }
});
