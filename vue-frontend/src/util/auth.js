/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
import router from '../router/index'
import {loginApi, registerApi} from './api-provider'
import {store} from './vue-store'
const axios = require('axios');

export function login(loginDetails) {
  const authUser = {}
  return loginApi(loginDetails)
    .then(function (res) {
      if (res.status === 200) {
        authUser.user = res.data
        authUser.token =
          res.headers["authorization"]
        store.state.isLoggedIn = true
        store.state.user = authUser
        window.localStorage.setItem('loggedUser', JSON.stringify(authUser))
        axios.defaults.headers.common['Authentication'] = String(getToken());
        router.push('/home')
      } else {
        store.state.isLoggedIn = false
        store.state.user = {}
        return res.data
      }
    }, function (res) {
      store.state.isLoggedIn = false
      store.state.user = {}
      return res
    })
}

export function register(registrationDetails) {
  const authUser = {}

  return registerApi(registrationDetails)
      .then(function (res) {
        if (res.status === 200) {
          authUser.user = res.data
          authUser.token =
              res.headers["authorization"]
          store.state.isLoggedIn = true;
          store.state.user = authUser;
          window.localStorage.setItem('loggedUser', JSON.stringify(authUser))
          axios.defaults.headers.common['Authorization'] = String(getToken());
          router.push('/home')
        } else {
          store.state.isLoggedIn = false
          store.state.user = {}
          return res.data
        }
      }, function (res) {
        store.state.isLoggedIn = false
        store.state.user = {}
        return res
      })
}

export function isLoggedIn() {
  return store.state.isLoggedIn
}

export function logout() {
  var app = this;
  localStorage.removeItem("loggedUser");
  router.push({name: 'Login'});
  store.state.isLoggedIn = false;
}

export function getLoggedUser() {
  var authUserString = window.localStorage.getItem('loggedUser')
  var authUser = JSON.parse(authUserString)
  return authUser
}

export function getToken() {
  var authUser = getLoggedUser()
  if (authUser != null) {

    return authUser.user.token
  }
  return null
}
