import {getToken, logout} from "./auth";

import axios from 'axios';
import router from "../router";


axios.defaults.headers.common['Authentication'] = String(getToken());

// axios.defaults.headers.common['Authorization'] = String(getToken());
axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    const originalRequest = error.config;
    if (error.response.status === 401 && !originalRequest._retry) {
        originalRequest._retry = true;
        logout()
    }
    return Promise.reject(error);
});



export function convertFile(fileStatusId, contentPath, outputFormat) {
    var initConversionRequest = {
        fileStatusId,
        contentPath,
        outputFormat
    };

    return axios.post('/conversion/convert', initConversionRequest);
}

export function downloadFile(fileStatusId){

    axios({
        url: `/files-core/download?id=${fileStatusId}`,
        method: 'GET',
        responseType: 'blob',
    }).then((response) => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement('a');

        fileLink.href = fileURL;
        fileLink.setAttribute('download', response.headers.filename);
        document.body.appendChild(fileLink);

        fileLink.click();
    });
}

export function register(){
    if (this.password === this.password_confirmation && this.password.length > 0)
    {
        let url = "http://localhost:3000/register"
        if(this.is_admin != null || this.is_admin == 1) url = "http://localhost:3000/register-admin"
        this.$http.post(url, {
            name: this.name,
            email: this.email,
            password: this.password,
            is_admin: this.is_admin
        })
            .then(response => {
                localStorage.setItem('user',JSON.stringify(response.data.user))
                localStorage.setItem('jwt',response.data.token)

                if (localStorage.getItem('jwt') != null){
                    this.$emit('loggedIn')
                    if(this.$route.params.nextUrl != null){
                        this.$router.push(this.$route.params.nextUrl)
                    }
                    else{
                        this.$router.push('/')
                    }
                }
            })
            .catch(error => {
                console.error(error);
            });
    } else {
        this.password = ""
        this.passwordConfirm = ""

        return alert("Passwords do not match")
    }
}

export function login(loginModel){
    axios.post("/security-center/login", loginModel).then(response => {
        let is_admin = response.data.user.is_admin
        localStorage.setItem('user',JSON.stringify(response.data.user))
        localStorage.setItem('jwt',response.data.token)

        if (localStorage.getItem('jwt') != null){
            this.$emit('loggedIn')
            if(this.$route.params.nextUrl != null){
                router.push(this.$route.params.nextUrl)
            }
            else {
                if(is_admin== 1){
                    this.$router.push('admin')
                }
                else {
                    this.$router.push('dashboard')
                }
            }
        }
    })
}


export function loginApi (loginDetails) {
    const url = `/security-center/login`;
    return axios.post(url, loginDetails).then(
        response => response
    )
}

export function registerApi (registrationDetails) {
    const url = `/security-center/register`;
    return axios.post(url, registrationDetails).then(
        response => response
    )
}

export function getStats() {
    const url = `/files-core/stats`;
    return axios.get(url).then(
        response => response
    )
}

export function getMyFiles() {
    const url = `/files-core/my-files`;
    return axios.get(url).then(
        response => response
    )
}

export function getAllUsers() {
    const url = `/user-management/users`;
    return axios.get(url).then(
        response => response
    )
}
