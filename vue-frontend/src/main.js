import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import VueClip from 'vue-clip'
import VueRouter from 'vue-router'
import router from './router'
import moment from 'moment'
import {store} from './util/vue-store'

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('MM/DD/YYYY hh:mm')
  }
});

Vue.use(VueClip)
Vue.config.productionTip = false


new Vue({
  store,
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
