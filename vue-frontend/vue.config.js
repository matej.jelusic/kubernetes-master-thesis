module.exports = {
  runtimeCompiler: true,
  "transpileDependencies": [
    "vuetify"
  ],
  devServer: {
    proxy: {
      "/*": {
        target: "http://192.168.64.48"
      }
    },
    port: 4200
  }
}