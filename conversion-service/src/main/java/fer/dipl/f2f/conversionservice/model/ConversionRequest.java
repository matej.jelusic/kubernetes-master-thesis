package fer.dipl.f2f.conversionservice.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ConversionRequest {

  @NotEmpty
  private Long fileStatusId;

  @NotEmpty
  private String contentPath;

  @NotNull
  private OutputFormat outputFormat;

  private String outputPath;
}
