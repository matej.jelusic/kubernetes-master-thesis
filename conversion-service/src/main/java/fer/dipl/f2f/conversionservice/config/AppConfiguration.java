package fer.dipl.f2f.conversionservice.config;

import fer.dipl.f2f.conversionservice.model.ServiceInfo;
import org.apache.tika.Tika;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
public class AppConfiguration {

  @Bean
  public Tika tika() {
    return new Tika();
  }

  @Bean
  public ServiceInfo serviceInfo() throws UnknownHostException {
    ServiceInfo serviceInfo = new ServiceInfo();
    serviceInfo.setServiceHostIp(InetAddress.getLocalHost().getHostAddress());
    serviceInfo.setServiceHostName(InetAddress.getLocalHost()
            .getHostName());
    return serviceInfo;
  }
}
