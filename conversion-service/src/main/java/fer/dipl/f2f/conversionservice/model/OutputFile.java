package fer.dipl.f2f.conversionservice.model;

import lombok.Data;

@Data
public class OutputFile {

  private String fileName;
  private String filePath;
}