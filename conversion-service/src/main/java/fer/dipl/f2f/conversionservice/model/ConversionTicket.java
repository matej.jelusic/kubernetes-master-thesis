package fer.dipl.f2f.conversionservice.model;

import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@ToString
public class ConversionTicket {

  private Long fileStatusId;
  private ServiceInfo serviceInfo;

}
