package fer.dipl.f2f.conversionservice.exception;

public class RuntimeExecException extends RuntimeException {

  public RuntimeExecException() {
  }

  public RuntimeExecException(String s) {
    super(s);
  }

  public RuntimeExecException(String s, Throwable throwable) {
    super(s, throwable);
  }
}
