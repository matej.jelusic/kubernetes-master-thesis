package fer.dipl.f2f.conversionservice.controller;


import fer.dipl.f2f.conversionservice.model.ConversionRequest;
import fer.dipl.f2f.conversionservice.model.ConversionResult;
import fer.dipl.f2f.conversionservice.model.ConversionTicket;
import fer.dipl.f2f.conversionservice.model.ServiceInfo;
import fer.dipl.f2f.conversionservice.service.ConversionRequestProcessor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class ConversionController {

    private final ConversionRequestProcessor conversionRequestProcessor;

    private final ServiceInfo serviceInfo;

    public ConversionController(ConversionRequestProcessor conversionRequestProcessor, ServiceInfo serviceInfo) {
        this.conversionRequestProcessor = conversionRequestProcessor;
        this.serviceInfo = serviceInfo;
    }

    @GetMapping("/")
    public String imHealthy() {
        return "{healthy:true}";
    }

    @GetMapping("/conversion")
    public String conversionHealthy() {
        return "{healthy:true}";
    }

    @PostMapping("conversion/convert")
    public ConversionResult conversionListener(@RequestBody ConversionRequest request){

        ConversionTicket conversionTicket = new ConversionTicket();
        conversionTicket.setServiceInfo(serviceInfo);
        conversionTicket.setFileStatusId(request.getFileStatusId());

        return conversionRequestProcessor.processConversionRequest(request, conversionTicket);
    }

}
