package fer.dipl.f2f.conversionservice.resource;

import fer.dipl.f2f.conversionservice.model.ConversionResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


public interface FilesCoreServiceProxy {

	@PostMapping("files-core/file-converted")
	public void fileConverted(@RequestBody ConversionResult conversionResult);
}