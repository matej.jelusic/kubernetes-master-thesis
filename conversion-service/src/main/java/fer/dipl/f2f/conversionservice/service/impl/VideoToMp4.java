package fer.dipl.f2f.conversionservice.service.impl;

import fer.dipl.f2f.conversionservice.model.OutputFormat;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;


@Service
public class VideoToMp4 extends CommandLineConverter {

  private List<String> supportedMimeTypes = new LinkedList<>();

  public VideoToMp4() {
    supportedMimeTypes.add("video/(.*)");
  }

  @Override
  protected String[] getCommandToRun(String absoluteInputPath, String absoluteOutputPath) {
    return new String[] {
        "ffmpeg",
        "-i",
        absoluteInputPath,
        absoluteOutputPath
    };
  }

  @Override
  public OutputFormat getOutputFileFormat() {
    return OutputFormat.MP4;
  }

  @Override
  public List<String> getSupportedMimeTypes() {
    return supportedMimeTypes;
  }
}