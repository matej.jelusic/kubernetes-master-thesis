package fer.dipl.f2f.conversionservice.service;

import fer.dipl.f2f.conversionservice.model.ConversionRequest;
import fer.dipl.f2f.conversionservice.model.ConversionTicket;
import fer.dipl.f2f.conversionservice.model.ConversionResult;
import fer.dipl.f2f.conversionservice.resource.FilesCoreServiceProxy;
import lombok.extern.log4j.Log4j2;
import org.apache.tika.Tika;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

@Service
@Log4j2
@Scope("prototype")
public class ConversionRequestProcessor {

    private final ConversionServiceLocator conversionServiceLocator;

    private final Tika tika;

    @Resource
    FilesCoreServiceProxy filesCoreServiceProxy;

    public ConversionRequestProcessor(ConversionServiceLocator conversionServiceLocator, Tika tika) {
        this.conversionServiceLocator = conversionServiceLocator;
        this.tika = tika;
    }

    public ConversionResult processConversionRequest(ConversionRequest request, ConversionTicket conversionTicket){
        ConversionService conversionService = conversionServiceLocator
                .locateConversionService(request.getContentPath(), tika.detect(request.getContentPath()),
                        request.getOutputFormat());

        Path relativeOutputPath = null;
        if (!StringUtils.isEmpty(request.getOutputPath())) {
            relativeOutputPath = Paths.get(request.getOutputPath());
        }

        log.info("Converting file: " + request.getContentPath() + " to file:" + relativeOutputPath);
        ConversionResult result = conversionService
                .convertFile(Paths.get(request.getContentPath()),
                        relativeOutputPath);

        result.setConversionTicket(conversionTicket);
        result.setDateConverted(new Date());
        System.out.println(conversionTicket);
        filesCoreServiceProxy.fileConverted(result);
        return result;
    }

    public String test() throws InterruptedException {
        log.info("Ušla dretva" + Thread.currentThread().getName());
        Thread.sleep(5000);
        return "alive";
    }

}
