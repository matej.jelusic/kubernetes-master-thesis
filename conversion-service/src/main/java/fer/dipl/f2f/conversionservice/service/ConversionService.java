package fer.dipl.f2f.conversionservice.service;


import fer.dipl.f2f.conversionservice.model.ConversionResult;
import fer.dipl.f2f.conversionservice.model.OutputFormat;

import java.nio.file.Path;
import java.util.List;

public interface ConversionService {

  ConversionResult convertFile(Path relativeInputPath, Path relativeOutputPath);

  OutputFormat getOutputFileFormat();

  List<String> getSupportedMimeTypes();
}