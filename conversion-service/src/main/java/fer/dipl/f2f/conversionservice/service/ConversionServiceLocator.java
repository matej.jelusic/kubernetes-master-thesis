package fer.dipl.f2f.conversionservice.service;

import fer.dipl.f2f.conversionservice.model.OutputFormat;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConversionServiceLocator {

  private final List<ConversionService> conversionServices;

  public ConversionServiceLocator(
      List<ConversionService> conversionServices) {
    this.conversionServices = conversionServices;
  }


  public ConversionService locateConversionService(String inputPath, String inputMimeType,
                                                   OutputFormat outputFormat) {
    return conversionServices.stream().filter(
        conversionService -> conversionService.getOutputFileFormat().getMimeType().equals(outputFormat.getMimeType())
            && conversionService.getSupportedMimeTypes().stream().anyMatch(inputMimeType::matches))
        .findFirst().orElseThrow(() -> new RuntimeException(
            "No matching conversion service found for input file[" + inputPath
                + "] output type[" + outputFormat + "]"));
  }
}
