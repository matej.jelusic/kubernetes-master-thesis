package fer.dipl.f2f.conversionservice.service.impl;


import fer.dipl.f2f.conversionservice.model.ConversionResult;
import fer.dipl.f2f.conversionservice.model.OutputFile;
import fer.dipl.f2f.conversionservice.service.ConversionService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Log4j2
@Service
public abstract class CommandLineConverter implements ConversionService {

  @Value("${file.system.base.url}")
  private String baseUrl;


  @Override
  public ConversionResult convertFile(Path relativeInputPath, Path relativeOutputPath) {
    Path absoluteInputPath = Paths.get(baseUrl).resolve(relativeInputPath).toAbsolutePath();

    Path absoluteOutputPath;
    if (relativeOutputPath == null) {
      absoluteOutputPath = absoluteInputPath.getParent();
    } else {
      absoluteOutputPath = Paths.get(baseUrl).resolve(relativeOutputPath);
    }

    absoluteOutputPath = absoluteOutputPath
        .resolve(getOutputFileFormat().getExtension().replaceAll("\\.", "")).resolve(
            absoluteInputPath.getFileName().toString().split("\\.")[0] + getOutputFileFormat()
                .getExtension());

    String[] command = getCommandToRun(absoluteInputPath.toString(),
        absoluteOutputPath.toString());

    int statusCode = 1;
    List<OutputFile> resultFiles = new LinkedList<>();
    String errorMessage = "Error running command: " + Arrays.toString(command);
    try {
      deleteDirectoryIfExists(absoluteOutputPath.getParent());
      createDirectoryIfNotExists(absoluteOutputPath.getParent());
      Runtime runtime = Runtime.getRuntime();
      log.info("Run command line: " + Arrays.toString(command));
      Process process = runtime.exec(command);
      statusCode = process.waitFor();
      Files.list(absoluteOutputPath.getParent()).forEach(path -> {
        Path outputPath = Paths.get(baseUrl).relativize(path);
        log.info("Created file:" + outputPath.toString());
        OutputFile outputFile = new OutputFile();
        outputFile.setFileName(outputPath.getFileName().toString());
        outputFile.setFilePath(outputPath.toString());
        resultFiles.add(outputFile);
      });
    } catch (IOException | InterruptedException e) {
      log.error(errorMessage);
      try {
        deleteDirectoryIfExists(absoluteOutputPath.getParent());
      } catch (IOException ex) {
        log.error("Cannot delete: " + absoluteOutputPath);
      }
    }

    ConversionResult conversionResult = new ConversionResult();
    conversionResult.setOutputFormat(getOutputFileFormat());
    conversionResult.setOutputFile(statusCode == 0 ? resultFiles.get(0) : null);
    conversionResult.setSuccess(statusCode == 0);
    if (statusCode != 0) {
      conversionResult.setErrorMessage(errorMessage);
      log.error(errorMessage);
    }

    return conversionResult;
  }

  private void deleteDirectoryIfExists(Path path) throws IOException {
    if (Files.exists(path)) {
      log.info("Delete directory: " + path);
      FileSystemUtils.deleteRecursively(path);
    }
  }

  private void createDirectoryIfNotExists(Path path) throws IOException {
    if (!Files.exists(path)) {
      log.info("Create directory: " + path);
      Files.createDirectories(path);
    } else if (!Files.isDirectory(path)) {
      log.info("Create directory: " + path);
      Files.createDirectories(path);
    }
  }

  protected abstract String[] getCommandToRun(String absoluteInputPath, String absoluteOutputPath);


  public String getBaseUrl() {
    return baseUrl;
  }

  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }
}