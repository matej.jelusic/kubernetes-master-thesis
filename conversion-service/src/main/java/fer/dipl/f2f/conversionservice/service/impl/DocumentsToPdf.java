package fer.dipl.f2f.conversionservice.service.impl;

import fer.dipl.f2f.conversionservice.model.OutputFormat;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;


@Service
public class DocumentsToPdf extends CommandLineConverter {

  private List<String> supportedMimeTypes = new LinkedList<>();

  public DocumentsToPdf() {
    supportedMimeTypes
        .add("application/vnd.openxmlformats-officedocument.presentationml.presentation");
    supportedMimeTypes
        .add("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    supportedMimeTypes.add("application/msword");
    supportedMimeTypes.add("application/vnd.ms-powerpoint");
  }

  @Override
  protected String[] getCommandToRun(String absoluteInputPath, String absoluteOutputPath) {
    return new String[]{
        "unoconv",
        "-f",
        "pdf",
        "-o",
        absoluteOutputPath,
        absoluteInputPath,
    };
  }

  @Override
  public OutputFormat getOutputFileFormat() {
    return OutputFormat.PDF;
  }

  @Override
  public List<String> getSupportedMimeTypes() {
    return supportedMimeTypes;
  }
}