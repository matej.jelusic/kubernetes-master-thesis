package fer.dipl.f2f.conversionservice.model;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ConversionResult {

  private ConversionTicket conversionTicket;

  private OutputFormat outputFormat;

  private boolean success;

  private String errorMessage;

  private OutputFile outputFile;

  private Date dateConverted;
}
